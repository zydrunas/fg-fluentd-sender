package com.telesoftas.fgmgw;

import org.fluentd.logger.FluentLogger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class FgMgwApplication {
    @Bean
    public FluentLogger fluentLogger() {
        return FluentLogger.getLogger("fg.test", "localhost", 24224);
    }

    public static void main(String[] args) {
        SpringApplication.run(FgMgwApplication.class, args);
    }
}
