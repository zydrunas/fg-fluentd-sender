package com.telesoftas.fgmgw.entity;

import java.util.Date;

public class FGTransaction {

    private String tag;
    private String uid;
    private String system;
    private Date date;
    private Double amount;
    private int type;

    public FGTransaction(String tag, String uid, String system, Date date, Double amount, int type) {
        this.tag = tag;
        this.uid = uid;
        this.system = system;
        this.date = date;
        this.amount = amount;
        this.type = type;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getSystem() {
        return system;
    }

    public void setSystem(String system) {
        this.system = system;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
