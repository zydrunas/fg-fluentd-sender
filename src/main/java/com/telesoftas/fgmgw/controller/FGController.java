package com.telesoftas.fgmgw.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.telesoftas.fgmgw.entity.FGTransaction;
import org.fluentd.logger.FluentLogger;
import org.msgpack.jackson.dataformat.MessagePackFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.*;
import java.util.*;

@RestController
public class FGController {

    @Autowired
    private FluentLogger fluentLogger;

    @GetMapping(value = "/get/shit/done")
    public String getPack() throws IOException {
        long timestamp = System.currentTimeMillis() / 1000;

        List<FGTransaction> src = new ArrayList<FGTransaction>();

        FGTransaction fgTransaction = new FGTransaction("fg.transactions","GJHGF-78Fg-GHFJE", "FG-GW-8Fg56Y", new Date(), 105.55, 'D');

        src.add(fgTransaction);

        ObjectMapper objectMapper = new ObjectMapper(new MessagePackFactory());
        byte[] bytes = objectMapper.writeValueAsBytes(src);

        ObjectMapper objectMapper2 = new ObjectMapper();

//        Map<String, Object> record = new HashMap<String, Object>();
//        record.put("uid", "GJHGF-78Fg-GHFJE");
//        record.put("system", "FG-GW-8Fg56Y");
//        record.put("amount", 105.55);
//        record.put("type", "C");

        Map<String, Object> map = objectMapper2.convertValue(fgTransaction, Map.class);

        fluentLogger.getSender().emit("fg.test", timestamp, map);


//        Socket socket = new Socket("fluentd-hostname", 24224);
//        DataOutputStream os = new DataOutputStream(new BufferedOutputStream(socket.getOutputStream()));
//        os.writeBytes("{\"message\": {\"someField\":\"someValue\"} }");
//        os.flush();
//        socket.close();


        // Deserialize the byte array to a List
        List<Object> deserialized = objectMapper.readValue(bytes, new TypeReference<List<Object>>() {});
        System.out.println(deserialized);

        return deserialized.toString();
    }
}
